---
layout: default
---
<style>
  table { width: 100%;}
  td { min-width: 50px; text-align: center; vertical-align: top; }
  figure {display: inline-block; margin: 0}
</style>

# An Analysis of Clearing Market Mechanisms
Although the term is used quite loosely, a "market mechanism" can be defined as some sort of mechanism to facilitate the exchange of goods between autonomous agents in which there is a concept of *prices* used to ration and regulate supply and demand of the goods. It is also typically assumed that consumers and producers in the market are anonymous. I.e. they trade with the market not with individuals, and make decisions based on market prices, not the identities of the other agents that happen to be participating. *Clearing* market mechanisms (also called tatonnement processes, Walrasian auctions, price adjustment procedures, double sided clearing auctions, ...) are one of the main classes of market mechanism. In clearing markets good are exchanged only when global prices (one price  per unit of each good) are established. Clearing market mechanisms can be interpreted and analyzed as dual decompositions of central optimization objectives. Interpreting clearing market mechanisms as dual decompositions gives us a foundational framework via which we can more clearly see the relationship between different mechanisms proposed in the literature, the scope for variation in the mechanisms, and the exact conditions under which a stable equilibrium exists.

The general model of market we consider is as follows. Let $D$ be the set of consuming agents, and $S$ be the set of producing agents in the market. It is assumed goods can be exchanged in arbitrarily small amounts. Let $G = {1..n}$  be an index set representing the $n$ goods. Let $x_d \in \mathbb{R}^n$ be the the $d$-th consumer's consumption bundle and $x\_s \in \mathbb{R}^n$ the $s$-th agents production bundle. Assume each consumer has a utility function over its consumption bundle $U\_d(x\_d):\mathbb{R}^n \rightarrow \mathbb{R}$ and each producer has a well-defined cost function over its production bundle $C\_b(x\_b): \mathbb{R}^n \rightarrow \mathbb{R}$. Furthermore, each consumer and producer has certain hard constraints on what they may consume or produce represented by constraint sets $\mathcal{X\_d}$ and $\mathcal{X\_s}$ respectively.

The central optimization objective is to find a balanced exchange of goods between producers and consumers that maximizes utility while simultaneously minimizing costs:

$$
\begin{align}
  max\ & \sum_d^D U(x_d) - \sum_s^S C(x_s)\label{eq-central-main}\ &\text{s.t.}\\
      & x_D \in X_D,\ x_S \in X_S\\
      & \sum_d^D x_d + \sum_s^S x_s = 0
\end{align}
$$

where:

$$
\begin{align}
\begin{split}
  x_D = [x_{1}^T, ..., x_{|A|}^T]^T\\
  \mathcal{X_D} = \mathcal{X_{1}} \times ... \times \mathcal{X_{|A|}}\\
  x_S = [x_{1}^T, ..., x_{|S|}^T]^T\\
  \mathcal{X_S} = \mathcal{X_{1}} \times ... \times \mathcal{X_{|S|}}\\
  \end{split}
\end{align}
$$

Supposing the utility functions are concave, the cost functions are convex, and all the constraint sets $\mathcal{X_d}$, $\mathcal{X_a}$ are convex, then a unique global optimal (market equilibrium) value exists and the above and can be solved central via convex optimization methods.

It's often desirable and more practical in a multiagent market setting to decompose the central problem. The idea behind any decomposition is to exploit the separable structure of a central objective to allow sovling it to be split up into indpendent calculations. Most clearing *market* mechanisms can be interpreted as variants of dual decompositions of the central objective (primal as opposed to dual decompositions are also possible but less clearly map to conventional "market" semantics). Market interactions in the form of exchanging bids and offers are effectively an implementation of distributed gradient descent where the market participants are doing the work to calculate a solution, coordinated in some way according to the market protocols.

There are two main dual decompositions of the above central problem: the classical Lagrangian dual decomposition (just dual decomposition herein), and by partitioning (which also involves Lagrangian duality). The following two sections discuss these two approaches and how the above central optimization objective can be decomposed according to each.

## Clearing Market Mechanisms via Dual Decomposition ##########
It is helpful to reorganize the central objective in ($\ref{eq-central-main}$) by treating the utility functions as cost functions (or in other words *disutility* functions): $C_d(x_d) = -U_d(x_d), \forall d$, and lumping the consumer and a producer sets together into one consumption or production agnostic set of market agents $A = D \cup S$. The optimization problem can then be rewritten:

$$
\begin{align}
  \min\ & \sum_a^A C_a(x_a)\label{eq-central-main-lumped}\ &\text{s.t.}\\
          & x_A \in \mathcal{X_a} & \forall_{a \in A}\\
          & \sum_a^A x_a = 0
\end{align}
$$

We'll call this presentation the *general form*, and the original one the *partitioned form* of the problem. In general form it easier to see the separable structure of the problem; without the balancing constraint, $\sum_a^A x_a = 0$, the problem decomposes into $\|A\|$ independent optimization problems. The idea behind dual decomposition is to solve the equivalent dual problem in which this constraint is converted into weighted penalties on the objective. The dual problem is a maximization problem of the dual function, $d(\lambda)$, which is a function of the dual variables, $\lambda$, which can be interpreted as Lagrangian (or more generally "geometric") multipliers related to the primal problem. The dual problem is:

$$
  \max_{\lambda \in \mathbb{R}^n} d(\lambda)
$$

where:

$$
\begin{align}
  d(\lambda) & = \inf_{x_a \in \mathcal{X_a},\ \forall a \in A}(\sum_{a}^A C_a(x_a) + \lambda^{T} \sum_a^A x_a)\\
\end{align}
$$

We can now exploit the separable structure inherent in the primal problem by decomposing the dual function into the sum of $\|A\|$ separate functions:

$$
\begin{align}
  \begin{split}
  d(\lambda) & = \inf_{x_a \in \mathcal{X_a},\ \forall a \in A}(\sum_{a}^A(C_a(x_a) + \lambda^{T} x_a))\\
  & = \sum_{a}^A(\inf_{x_a \in \mathcal{X_a}}(C_a(x_a) + \lambda^{T} x_a))\\
  & = \sum_a^A d_a(\lambda)
\end{split}
\end{align}
$$

where:

$$
  d_a(\lambda) = \inf_{x_a \in \mathcal{X_a}}(C_a(x_a) + \lambda^{T} x_a)
$$

and the dual problem can be rewritten:

$$
  \max_{\lambda \in \mathbb{R}^n} \sum_a^A d_a(\lambda)
$$

### Computational Methods for Solving The Decomposed Lagrangian Dual $\label{s-comp-methods}$
The domain of the dual function turns out to be convex and the dual function is concave regardless of the convexity of the primal problem [[Bertsekas]][Bertsekas1999Nonlinear]. Under certain qualifying assumptions on the primal problem $d(\lambda)$ is also differentiable and can be solved via simple ascent direction methods such as steepest gradient ascent method:

$$
  \lambda^{k+1} = \lambda^{k} + a^{k} s(\lambda)
$$

where $a^k$ is a step size, $s(\lambda)$ is the gradient. In some cases, while $d(\lambda)$ is concave it is not differentiable. In this case $s(\lambda)$ can be replaced by a sub-gradient of $d(\lambda)$ and the above method will yield an close solution given a small enough step size [[Palomar]][Palomar2006].

It turns out that, *in the problem at hand*, the gradient (or a sub-gradient if the gradient does not exist) of $d$ is given simply by $d$ itself. In order to evaluate $d$ at a given $\lambda$ we effectively need to solve $\|A\|$ minimization problems. Listed out verbose the steepest ascent algorithm for the given problem is:

<ol>
  <li>$\lambda^0$ = initialize-to-some-value</li>
  <li>$k = 0$</li>
  <li>Do:
    <ol>
      <li>$d^k = \sum_a^A d_a(\lambda^k)$</li>
      <li>$\lambda^{k+1} = \lambda^{k} + a^k d^k$</li>
      <li>$k = k+1$</li>
    </ol>
  </li>
  <li>While $d^k \ne 0 \pm \epsilon$</li>
</ol>

In practice, this algorithm is carried out by a central (assumed unbiased) market auctioneer who makes requests to market participants; step 3.1 corresponds to a remote call to each of the $\|A\|$ agents who are responsible for calculating the value $d_a(\lambda^k)$. In fact, this algorithm is identical the standard price adjustment procedure alluded to by Walras and rigorously analyzed by Samuelson and other early mathematical economists (who did not invoke Lagrangian duality in their analysis). In a microeconomic sense the function $d\_a(\lambda)$ is the agent's demand function.

Since we are merely solving a concave maximization problem we can explore other well known gradient methods, beyond steepest ascent list above. Step 3.2 may be generalized to:

$$
\lambda^{k+1} = \lambda^{k} + a^k D^k d^k
$$

where $D^k$ is an $n$ by $n$ matrix. It is the selection of a scaling matrix $D^k$ and also step size $a^k$ that distinguishes one method from another. Newton's method is a popular variation in which case $D^k = (\nabla d^k)^{-1}$. However calculating the Jacobian of the dual function requires additional gradient information from the market agents. The inner loop of the algorithm above becomes:

<ol>
  <li>($d^k, \nabla d^k) = (\sum_a^A d_a(\lambda^k), \sum_a^A \nabla d_a(\lambda^k))$</li>
  <li>$\lambda^{k+1} = \lambda^{k} + a^k d^k (\nabla d^k)^{-1}$</li>
  <li>$k = k+1$</li>
</ol>

Line 1 indicates that in addition to the value of $d\_a(\lambda^k)$ each remote agent also returns the Jacobian of that function, $\nabla d_a(\lambda^k)$. This supposes all the Jacobians exist which may not be the case. Even if they do, it must be kept in mind, the calculation is performed by the remote agents. There are a number of practical disadvantages to this. Briefly:

  - The calculation may be computationally expensive for remote agents.
  - It may be difficult to derive an exact formula for the Jacobian.
  - It increases reliance on remote agents for proper operation of the overall mechanism.
  - It's unclear whether there are strategic robustness consequences.

In many of the popular alternatives to Newtons method, $D^k$ can be viewed as providing an approximate of the Jacobian, in order reduce computation or to provide an alternative in the case that it does not exist (analogous to using a sub-gradient instead of a gradient of a non-differentiable function in steepest descent).

**Incremental Gradient Methods:** Step 3.1 in the above algorithm need not be carried out synchronously and exactly once for each agent. Any method that only selects a subset of the agents to update in each iteration is called an incremental gradient method. Numerous alternatives are available. A round robin or random selection being the most obvious and popular. An incremental method may be appealing for various practical reasons. [[Bertsekas]][Bertsekas1999Nonlinear] suggest random incremental updates may converge more quickly on average (also stating proving this with any generality is difficult).

**Proximal Penalties:** One issue with any of the computational methods for solving the dual so far discussed is that if any of the cost (/disutility) functions of the primal problem are linear $d$ will not be differentiable. In such cases adding a "convexifying" proximal penalty is a simple fix and will ensure convergence. Since the master knows nothing about the shape of the sub-problems' constraints it is the far-side the incorporates the proximal penalty:

$$
  d_a(\lambda^k) = \inf_{x_a \in \mathcal{X_a}}(C_a(x_a) + (\lambda^k)^{T} x_a + \frac{1}{c^k}||x_a^{k-1} - x_a||^2)
$$

Note we have introduced yet another tuning parameter $c^k$ which weights the penalty. Choice of $c^k$ can obviously impact the rate of convergence and the selection of which is generally not independent of the selection strategy for $a^k$ and $D^k$.

**Function Bid vs Point Bid Submission**
Another way to look at the case where $D^k = I$ (steepest descent) and when it doesn't is to consider the first case a "point bid" and the latter case a "function bid" [[J.Chang & P.Wellman]][WALRAS]. Here the function is supposed to be an approximation of an agent's deman function in the vicinity about $lambda^k$. In the Newtonian case described above $D^k$ is in fact the first order approximation of this function, but generalizing this further to some arbitrary approximation in some arbitrary form opens up myriad new, but possibly less rigorously defined approaches.

## Clearing Market Mechanisms via Partitioning ##########
Partitioning is a type of decomposition also relying on duality that splits (partitions) the central problem in two. It is applicable to problems of the form:

$$
\begin{align}
  \min {} & F(x) + G(y) \label{eq-meta-master}\ \text{s.t.}\\
          & x \in X, y \in Y\\
          & A x + B y = c
\end{align}
$$

where $F:\mathbb{R}^n \rightarrow \mathbb{R}$ and $G:\mathbb{R}^m \rightarrow \mathbb{R}$ are convex functions, the constraint sets, $X$ and $Y$, are convex, and $A x + B y = c$ denotes some set of $r$ linear constraints [[Bertsekas]][Bertsekas1999Nonlinear]. The last constraint couples the variables $x$ and $y$. The main motivation behind partitioning is the same as in dual decomposition; to relax the coupling constraint in order to exploit the separability between the two terms in the objective function. Once this done, dual decomposition (as covered in the section above) can potentially be employed on each side of the partition.

Under the above convexity assumptions, the problem in ($\ref{eq-meta-master}$) can be restated in terms of a "master-problem" in variables $x$ and a "sub-problem" in variables $y$ [[Bertsekas]][Bertsekas1999Nonlinear]:

$$
\begin{align}
  \min {} & F(x) + p(c - Ax)\label{eq-master-s}\ \text{s.t.}\\
          & x \in X
\end{align}
$$

where:

$$
\begin{align}
  p(u) = \inf_{B y = u, y \in Y} G(y) \label{eq-inf-1}
\end{align}
$$

The sub-problem can be solved via its equivalent Lagrangian dual problem as discussed above. It also turns out the sub-gradients of $p(u)$ are given by the set of all $-\lambda$ where $\lambda$ is a geometric multiplier of the dual problem:

$$
\begin{align}
  p(u) & = \max_{\lambda \in \mathbb{R}^r} (\inf_{y \in Y} (G(y) + \lambda^T(By - u)))
\end{align}
$$

The sub-gradients of $p(u)$ can used in solving the master-problem via projected gradient method. At each step we solve for $p(c - Ax)$ at a given $x$, then use the resulting sub-gradient information to perform one iteration on the master problem. The iteration is:

$$
\begin{align}
  x^{k+1} = [-\alpha^k(\nabla F(x) + \nabla_x p(c - Ax))]^X
\end{align}
$$


Applied to out original clearing market problem in partitioned form, the master-problem is:

$$
\begin{align}
  min\ & \sum_d^D C(x_d) + \sum_s^S C(x_s)\ &\text{s.t.}\\
      & x_D \in \mathcal{X_d},\ x_s \in \mathcal{\mathcal{X_s}}\\
      & \sum_d^D x_d + \sum_s^S x_s = 0\\
  = &\\
  min\ & \sum_d^D C(x_d) + p(-\sum_d^D x_d)\ &\text{s.t.}\\
      & x_d \in \mathcal{X_d} & \forall_{d \in D}\\
\end{align}
$$

where:

$$
\begin{align}
  \begin{split}
  p(u) & = \inf_{\sum_s^S x_s = u} \sum_s^S C_s(x_s) \label{eq-na-li-inf-1}\\
  & = \max_{\lambda \in \mathbb{R}^r} (\inf_{s \in S} (\sum_s^S C_s(x_s + \lambda^T(\sum_s^S x_s - u)))\\
\end{split}
\end{align}
$$

The gradient descent step on the master-problem is:

$$
\begin{align}
  x^{k+1} = [-\alpha^k(\nabla C(x) + \nabla_x p(\sum_d^D x_d)]^X
\end{align}
$$

which having solved the sub-problem and thus for $\nabla_x p(\sum_d^D x_d)$ is itself decomposable into $\|D\|$ operations:

$$
\begin{align}
  x_{d}^{k+1} = [-\alpha^k(\nabla C_d(x_d) + \nabla_{x_d} p)]^{\mathcal{X_d}}
\end{align}
$$

In solving the sub-problem or master-problem via gradient methods, the variations discussed in section sub section $\ref{s-comp-methods}$ are typically viable and can potentially be used.

## Dual Decomposition vs Partitioning
Which approach is best is an open question and depends on the particular market structure being considered. Partitioning approaches require the market be structurally split into a supply and demand side. This prohibits consumers from also being producers (prosumers). On the other hand doing so may allow us to exploit the particular structure of the market. For example, if there is only one supplier, it may be possible to solve the sub-problem side of the partition very quickly, in a manner not requiring any decomposition. The main advantage of the dual decomposition of the clearing market problem is that it treats the supply and demand side agnostically, thus potentially supporting prosumption.

[Palizban2014]: http://www.sciencedirect.com/science/article/pii/S1364032114000264
[Gu2014]: http://www.sciencedirect.com/science/article/pii/S0142061513002883
[Jongerden2009]: https://ieeexplore.ieee.org/abstract/document/5346550
[Li2011]: https://ieeexplore.ieee.org/abstract/document/6039082/
[Xiaoping2010]: /
[Zhang2013Robust]: https://ieeexplore.ieee.org/abstract/document/6510507/
[Palomar2006]: https://www.princeton.edu/~chiangm/decomptutorial.pdf
[Bertsekas1999Nonlinear]: http://www.athenasc.com/nonlinbook.html
[WALRAS]: https://link.springer.com/content/pdf/10.1023/A:1008654125853.pdf
