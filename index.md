---
layout: default
permalink: /
---
<style>
  table { width: 100%;}
  td { min-width: 50px; text-align: center; vertical-align: top; }
  figure {display: inline-block; margin: 0; text-align: center; }
</style>
# A Preliminary Numerical Evaluation of Three Clearing Market Mechanisms for Microgrids

# Abstract
This brief preliminary investigation provides numerical comparative evaluation of three clearing market mechanisms; the [Li et al][Li2011] mechanism (LCL) and two dual decomposition based mechanisms; Standard Price Adjustment (SPA) and Newton Price Adjustment (NPA) procedures. We limit ourselves to using the case study scenario presented in [Li et al][Li2011] in the evaluation, which is a day-ahead electrical energy only trading scenario on a small electrical network.

The LCL method performs best in the specific scenario because it exploits partitioning (market is partitioned into supply and demand side) to match the structure of the case study scenario. The other two are based on more general dual decomposition (the difference is explained in detail [1][1]). This means while the LCL method performs better under this scenario it can't be used to solve more general market scenarios (prosumption) that the other two can. Some further discussion of this is provided in the discussion section and appendix.

# Overview
One of our main research aims is to devise practical market mechanisms for energy trading in microgrids. Clearing market mechanisms are one major paradigm for market mechanisms. A clearing market mechanism finds equilibrium prices for many commodities at which aggregate supply and demand between market participants is balanced. At these prices commodities are then exchanged. In the context of microgrid energy trading, the traded commodity is energy consumption in a discrete intervals of a finite future time horizon. The time horizon considered is typically a day-ahead and the interval in the order of 5 to 60 minutes.

There are a number of practical requirements a microgrid energy trading market mechanism should meet. The complete list is long and hard to state in absolutes (i.e. it's a matter of engineering trade offs). However, two broad desirata are; 1. steady and fast convergence to a good solution, and 2. support for arbitrary peer to peer energy exchange. This second requirement means the market participants should be able to be "prosumers" and exchange energy, not just buy it.

One of the main clearing mechanisms we've used as a reference early in this project is that of [Li et al][Li2011]. The mechanism can be shown to converge quite fast. However, as presented in [Li et al][Li2011], the mechanism makes structural assumptions about the market participants and is limited in it's ability to support prosumption. The background analytical report in [1][1] shows how this mechanism may be interpreted as a *partitioning decomposition* of a central convex optimization objective. That report also describes the well known alternative; clearing market mechanisms based on *dual decomposition*. These actually correspond to the more conventional "tatonnment price adjustment" mechanisms found in microeconomics texts.

This very brief preliminary investigation provides numerical comparative evaluation of three clearing market mechanisms; the [Li et al][Li2011] mechanism (LCL) and two dual decomposition based mechanisms; Standard Price Adjustment (SPA) and Newton Price Adjustment (NPA) procedures. We limit ourselves to using the case study scenario presented in [Li et al][Li2011] in the evaluation, which is a day-ahead electrical energy only trading scenario on a small electrical network.

The main objective of the investigation is to compare rate of convergence of the different mechanisms. A secondary aim is merely to validate our implementation and get a feel for the viability of the obvious alternatives to the LCL mechanism. Given the very limited scope of the numerical evaluations no general assertions can be made from the results. However, the LCL mechanism is shown to converge much faster under the particular parameter settings used. While the other mechanisms are slower to converge, the convergence rate may be satisfactory given the advantage of supporting arbitrary peer to peer exchanges. It is also possible future improvements to convergence rate could be made.

A very brief overview of the underlying system setup and the three clearing market mechanisms is given in the next section. However, it is assumed the reader is familiar with market mechanisms and how they are intended to be used for day-ahead energy trading in microgrids.

## System Model Overview
There is a microgrid owner/operator type role who also supplies electrical energy to a collection of tenants (who are also market agents) on the microgrid network. Tenants have demand response capabilities, an energy storage (battery), and possibly also energy generation assets (in the actual case study we do not include any generation because it is not present in the reference case study). The electrical network is considered lossless and unconstrained, while tenant have a "fuse box" constraint limiting total import power, and prohibiting energy export to the network (i.e. that are strictly consumers). A clearing market mechanism is in place which tenants and the microgrid owner/operator use to agree to a mutually beneficial clearing market prices for energy at each interval of a finite future planning horizon by repeatedly exchanging offers and bids (in a mechanism specific form - see below) until equilibrium prices are reached. W.l.o.g. we assume the interval is one hour and the horizon is the "day-ahead" starting 08:00.

We assume the supplier has a convex cost function and all consumers have concave utility functions describing their preferences for production / consumption bundles. From a mathematical stand point a clearing market mechanism can be viewed as a distributed way of solving a central utility maximization and cost minimization objective and these (possibly unrealistic) convexity assumptions ensure the objective has a global optimal and that the mechanisms converge.

## Mechanisms
The LCL mechanism can be classified a special case of a partitioned decomposition of the central optimization objective, while SPA and NPA can be viewed classical dual decompositions with different update rules. Refer to [the linked report][1] for a detailed explanation on partitioning and dual decomposition and [Li et al][Li2011] for a description of their mechanism. In brief the SPA and NPA work like this:

**SPA:**

    do:
      demand = get_bids_from_agents(price)
      price = adjust_price(sum(demand))
    while demand != 0 +-err

**NPA:**

    do:
      (demand, jacobians) = get_bids_from_agents(price)
      price = adjust_price(sum(demand), sum(jacobians))
    while demand != 0 +-err

The only difference with the SPA and NPA is in the latter agents supply additional gradient information (the Jacobian), instead of just "point bids". The central auctioneer uses the gradient information to speed up (in theory) gradient descent (exactly the same way a Newtonian method works relative to steepest descent method). Newton's method is also in theory less sensitive to step size selection, as the method could be interpreted as a type of dynamic step size selection method. The price update rule in the two methods are effectively as follows [[Sandholm]][Sandholm]:

  - Standard: $p^{k+1} = p^k + s^k d(p^k)$
  - Newton: $p^{k+1} = p^k + s^k d(p^k) (\nabla d(p^k))^{-1}$

where $s^k$ is the step size, $d(p^k)$ is the demand function at price $p^k$. In the Newton version it is common to set the step size to a fairly large constant value around ~ $[0.5, 1]$, as the $\nabla d(p^k))^{-1}$ term entails an adaptive scaling of response based on first order conditions at $d(p^k)$. Note $d(p)$ is the sum of the market agent demand functions, $d(p) = \sum_a^A d_a(p)$. The exact form of $d_a(p)$ is defined in the [accompanying report][1].

We also need to introduce a proximal penalty into the $d_a(p)$ calculation for stablity of SPA and NPA (this is discussed in the [accompanying report][1]). It should be noted, introducing proximal penalties means we cannot assume the market participants are conventional price takers, as they do not respond to price rationally. However, the LCL mechanism *shares this issue*. An interesting question is how a mechanism could be designed such that proximal behavior corresponds to that of a rational price taking agent, but exploring this is beyond the scope of this report.

# Numerical Experiments

## Setup
In all tests I limited number of iterations to 200. I required a +/-5W excess demand at all time intervals for convergence. It should be noted the convergence test is different for LCL (by necessity) and the solution for SPA, NPA at a given tolerance should be slightly more accurate. The difference was assumed negligible.

For NPA and SPA there are two tuning parameters, the stepsize $s^k$ and proximal penalty weight $c^k$. For LCL the only tuning parameter is stepsize.

**Stepsize:** The best step size varies for each method on different scenarios. LCL is not particularly sensitive to step size selection when used with limited minimization (as we've done here). NPA should also be less sensitive to step size selection. To converge SPA requires a small stepsize. Instead of using a fixed small step size, I used a diminishing stepsize rule. To get a fair comparison, by trial and error and using some expert intuition, I found three step sizes for each method that should converge while also converging relatively fast. The stepsize used are:

  - LCL: 1/2, 1/5, 1/10
  - NPA: 1/2, 1/5, 1/10
  - SPA: Diminishing: (1/(k+s)) where s = 5,20,100

**Proximal Penalty Weight:** For $c^k$ I just used a constant $1/2$ (better convergence maybe achievable with a different policy).

## Results
**Main observations:**

  - The table below shows the main result. In all cases an equilibrium price was found. The best convergence rate in number of steps (rounds of the market) for LCL, SPA, NPA were 33, 130, 129. The LCL mechanism converged fastest in all cases, and in about a 1/4 as many steps.
  - NPA converges only one step faster than SPA in the best case. NPA shows more variance over the range of stepsize constants then SPA.
  - Figure 1 shows the average market excess with number of steps for the best case from table 1 for the three methods. The figure shows SPA convergence has a long tail, and actually converges close to equilibrium quite quickly. Although NPA converged one step faster then SPA overall, the average excess fluctuates around equilibrium significantly more.
  - Figures 2, 3 show the equilibrium market prices and demands, again for the the best cases. Overall the correspondence is quite close. The LCL result appear to diverge slightly from the other mechanisms in places. This divergence is very likely due to the non-equivalent market termination condition.

---

|method|stepsize|steps|excess (avg)|price (avg)|lf|utility (tot)|
|------|--------|-----|------|-----|--|-------|
|lcl|0.1|48|-0.000|0.194|0.788|9355.634|
|lcl|0.2|**33**|-0.000|0.194|0.795|9355.441|
|lcl|0.5|35|-0.000|0.194|0.802|9356.730|
|spa|(1/(100+s))|163|-0.001|0.193|0.806|9357.595|
|spa|(1/(20+s))|139|-0.001|0.193|0.807|9357.526|
|spa|(1/(5+s))|**130**|0.000|0.194|0.803|9356.372|
|npa|0.1|193|0.000|0.193|0.807|9355.920|
|npa|0.2|201|-0.001|0.194|0.811|9356.431|
|npa|0.5|**129**|0.000|0.194|0.809|9356.105|

---

<figure style="width: 100%">
    <img style="max-width:50%" name='f1' src='img/avg-excess.png'><br/>
    <small>Average market excess with steps (a step is a bid/offer round of the market). </small>
</figure>

---

<table>
  <tr>
    <td>
        <figure>
            <img style="width:100%" name='f1' src='img/demand.png'/><br/>
            <small>Demands</small>
        </figure>
    </td>
    <td>
        <figure>
            <img style="width: 100%" name='f2' src='img/prices.png'/><br/>
            <small>Prices</small>
        </figure>
    </td>
  </tr>
</table>

**Discussion:**

The similar performance between NPA and SPA is surprising, although maybe not too surprising. The NPA implementation was taken verbosely from that listed in [Sandholm][Sandholm]. In this implementation the calculation of the Jacobian does not take account of active constraints. While the method successfully converges, it is possible, due to active constraints there is a large degree of "diagonalization". The implementation could be improved to deal with this, but it is currently unclear how this would be done. On the other hand the performance of NPA may improve with diminishing stepsize.

The effect of the proximal penalty weighting has on results is unclear. It is possible a different constant weighting factor may improve results. It is possible there is a better well known dynamic weighting strategy that is going perform better in general. We have to keep in mind how practical it is to fine tune parameters such as this by trial and error in a real application however.

**Conclusions:**

  - SPA with diminishing stepsize is a reasonable alternative to LCL and unlike LCL can be used as a basis for arbitrary peer to peer exchanges. There is not a strong argument to use NPA over SPA given the comparative performance, and significantly increased computational requirements of NPA.
  - If pursuing clearing mechanisms and specifically NPA further, it may be worthwhile to repeat the experiments using a diminishing step size with NPA. Different, possibly variable proximal penalties could be explored.

---

<table>
  <tr>
    <td>
        <figure>
            <img width='380px' name='f1' src='img/lcl-lcl-ss=0.2-report-animation.gif'><br/>
            <small>LCL</small>
        </figure>
    </td>
    <td>
        <figure>
            <img width='380px' name='f2' src='img/lcl-npa-ss=5-report-animation.gif'><br/>
            <small>SPA</small>
        </figure>
    </td>
    <td>
        <figure>
            <img width='380px' name='f3' src='img/lcl-npa-ss=2-report-animation.gif'><br/>
            <small>NPA</small>
        </figure>
    </td>
  </tr>
</table>

---

# Appendix

## Additional Numerical Experiments

### No Proximal Penalty
The GIFs below shows the instability that occurs without the proximal penalty in the NPA and SPA. The reason for the instability is linearities in the underlying utility function, which in short, lead to infinite price elasticity. This only occurs when the central problem is only weakly convex and not strongly convex.

<table>
  <tr>
    <td>
        <figure>
            <img style="width:100%" name='f2' src='img/lcl-standard-ss=0.005-report-animation.gif'><br/>
            <small>./lcl-standard-ss=0.005-report-animation.gif</small>
        </figure>
    </td>
    <td>
        <figure>
            <img style="width:100%" name='f3' src='img/lcl-newton-ss=0.01-report-animation.gif'><br/>
            <small>./lcl-newton-ss=0.01-report-animation.gif</small>
        </figure>
    </td>
  </tr>
</table>

---

### Adding PV and Allowing Prosumption
I tweaked the reference scenario used above to:

  1. Allow exports by tenants.
  2. Every even (4 of 8) homes get a 3kW solar panel.
  3. The owner operator operates a battery and a 10kW solar panel.

The table below show the equivalent results as in table 1 above for the new scenario, but only for SPA this time. As can be seen two of the homes become exporters during the day; these households are working type households so have no use for the excess energy. The table shows the average clearing price is much lower, demand is lower, and the aggregate utility has increased.

The images below show the load profiles for the overall network as they converge towards equilibrium for, home 1 (stay at home, no solar), and home 6 (working, solar).

---

|method|stepsize|steps|excess|price|lf|utility|
|------|--------|-----|------|-----|--|-------|
|pv-spa|0.2|92|0.000|0.072|0.529|9410.406|

---

<table>
  <tr>
    <td>
        <figure>
            <img style="width:100%" name='f2' src='img/lcl-pv-spa-ss=5-report-animation.gif'><br/>
            <small>Network</small>
        </figure>
    </td>
    <td>
        <figure>
            <img style="width:100%" name='f3' src='img/lcl-pv-spa-ss=5-report-animation-home1.gif'><br/>
            <small>Home 1</small>
        </figure>
    </td>
    <td>
        <figure>
            <img style="width:100%" name='f3' src='img/lcl-pv-spa-ss=5-report-animation-home6.gif'><br/>
            <small>Home 6</small>
        </figure>
    </td>
  </tr>
</table>

---


[1]: ./clearing-mechanisms-as-decompositions-analysis-and-review
[Li2011]: https://ieeexplore.ieee.org/abstract/document/6039082
[Sandholm]: http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.70.8213&rep=rep1&type=pdf
