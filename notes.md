# Analysis
The Newtonian price adjustment mechanism is somewhat tersely described in [Sandholm1999][Sandholm1999] which is the main reference for this implementation. It is worth mentioning that "price adjustment mechanisms" in microeconomics circles are effectively equivalent to "dual decomposition methods" in ~engineering and numerical analysis (the difference is usuallly in emphasis on which qualities are important in a given mechanism / method). [Bertsekas1999][Bertsekas1999Nonlinear] provides some discussion from the latter perspective.

## Dual Decompositions and Price Adjustment
Consider the following problem:

$$
  min \sum_i^A F_i(x_i)\ s.t. \sum_i^A x_i = 0, x_i \in X_i \forall i
$$

Where $A$ is the set of agents in the market, and $X\_i$ is a (presumed) convex constraint space for the i-th agent. A price adjustment mechanism can be interpreted as effectively solving this problem via a Lagrangian dual decomposition.

[Bertsekas1999][Bertsekas1999Nonlinear] (chapter 6.1) discusses dual decompositions and provides some insight into second order decompositions. Bertsekas states "under the particularly favorable circumstances that the dual function is twice differentiable", Newton's methods can be used to solve the dual. However, the analysis and example following, only considers the unconstrained case, i.e. $X\_i - \mathbb{R}^n,\ forall i$. Which leaves doubt as to how to proceed when this is not the case. Analytically, it seems clear pure Newtonian price adjustment will on work in unconstrained case. Experimentally results support this (i.e. it works when certain constraints are not active). But what happens when we add constraints. Experimental results show it still works. However it may be the case that the Jacobians provided are actually a poor approximation of the true gradient and we could do much better.

Linear cost functions is another, separate problem wfor price adjustment mechanisms for which proximal penalties are a (but not necessarily the only) solution.

## Proximal Penalty for Descent Methods
Both the standard and Newton price adjustment mechanism (and AFAIK any similar dual decomposition mechanism) are only guaranteed to converge on a unique global optimal if demand curves are strictly convex. This means linear and uniform utility is an issue for these methods. At close to an optimal point the demand profile flip flops wildly with very small price changes. This is due to non strictly convex demand curves which effectively result in an extreme (infinite) sensitivity to very small changes in prices.

The issue is briefly raised in [Bertsekas1999][Bertsekas1999Nonlinear], p614. The general solution sugggested is a proximal penalty, although an exact fix is not clearly spellled out.

The proximal term means that when agents are asked for their demand at given price they don't necessarily return their true demand (in fact LCL works like this also - see note below). An artificial penalty is added to penalize very large changes in consumption. If the agent is asked for demand at the same price repeatedly, eventually the returned value will descend on the true demand value (LCL works like this also). In this way the proximal term acts like a dampener on large changes in demand profile.

There are different possible choice for the proximal penalty but it shold always be strictly convex and differentiable. The most common choice is the euclidean distance. The demand function of agent is normally given by:

        d(p_{k+1}) = x_{k+1} = argmin_x (C(x) + p_{k+1}*x)

where, p\_{k+1} and x\_{k+1} are the prices and demand at the k+1 step. With the proximal penalty this becomes:

        d(p_{k+1}) = x_{k+1} = argmin_x (C(x) + p_{k+1}*x + 1/(2*c_k)||x-x_k||^2)

where x\_k is the last demand, c\_k is a weigthing term that may diminish, grow or stay the same as k increases.

### Note on Na Li and Proximal Methods
In fact, the Na Li mechanism can be view as implementing a type of proximal method of sorts in that extreme movements from the current point is are dampened. There is an issue here (raised earlier) that such behaviour is not individually rational from a game theoretical perspective. An interesting question is whether it can be made to be, in a similar way to the VCG family of mechanisms. Another interesting queestion is the underlying relationship to continuous rather than clearing processes.
